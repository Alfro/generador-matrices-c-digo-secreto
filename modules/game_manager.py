import random
from modules.matrix_manager import MatrixManager

class GameManager:

    def __init__(self, seed=None):
        self.seed = seed
        if seed == None:
            self.seed = str(random.getrandbits(128))
        random.seed(self.seed)
        self.color_tiles = self.__gen_color_tiles()
        self.matrix_manager = MatrixManager()
        self.rows = 5

    ### Gen number of color tiles
    # It can be 8 or 9 red cells but not the same number as #187795, that could be 8 or 9 too.
    def __gen_color_tiles(self):
        color_tiles = {"#B4656F": 0, "#187795": 0, "#C7F2A7": 7, "#0D1B1E": 1}
        color_tiles["#B4656F"] = random.randint(8,9)
        print(color_tiles["#B4656F"])
        if color_tiles["#B4656F"] == 8:
            color_tiles["#187795"] = 9
        else:
            color_tiles["#187795"] = 8

        return color_tiles

    ### Which team start first?
    # The team with more tiles goes first
    def whos_first(self):
        if self.color_tiles["#B4656F"] == 9:
            first_team = "#B4656F"
        else:
            first_team = "#187795"

        return first_team

    def get_matrix(self):
        sequence = self.matrix_manager.gen_color_ordered_sequence(self.color_tiles)
        shuffled_sequence = self.matrix_manager.shuffle_sequence(sequence)

        return self.matrix_manager.chunk_it(shuffled_sequence, self.rows)
