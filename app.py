from flask import Flask, render_template, request
from modules.game_manager import GameManager

app = Flask('generacodigo')
@app.route('/')
def index():
    seed_value = request.args.get('seed')
    game_manager = GameManager(seed_value)
    matrix = game_manager.get_matrix()
    whos_first = game_manager.whos_first()
    return render_template('index.html', first_team=whos_first, matrix=matrix, seed=game_manager.seed)

if __name__ == '__main__':
    app.run()
